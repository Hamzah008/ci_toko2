<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class jenis_barang_model extends CI_Model
{
	//panggil nama table
	private $_table = "jenis_barang";
	
	public function tampilDatajenisBarang()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDatajenisBarang2()
	
	{
		$query = $this->db->query("SELECT * FROM jenis_barang WHERE flag = 1");
		return $query->result();
	}
	
	public function tampilDatajenisBarang3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_jenis', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}

	public function save()
	{
		
		$data['kode_jenis']	= $this->input->post('kode_jenis');
		$data['nama_jenis']	= $this->input->post('nama_jenis');
		
		$data['flag']	= 1;
		$this->db->insert($this->_table, $data);
	}
	
	
	public function detail ($kode_jenis)
	{
		$this->db->select('*');
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	
	public function update($kode_jenis)
	{
		$data['nama_jenis']	= $this->input->post('nama_jenis');
		$data['flag']	= 1;
		
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_jenis)
	{
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->delete($this->_table);
		
	}
	
	
	

}
