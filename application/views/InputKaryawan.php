
<table width="47%" border="0" cellspacing="0" cellpadding="5" bgcolor="green">


  <tr><td><center><b>Edit Karyawan</b></center><br/><br/></td></tr>
  <form action="<?=base_url();?>Karyawan/input" method="POST">
  <tr>
    <td width="47%">NIK</td>
    <td width="3%">:</td>
    <td width="50%">
      <input type="text" name="nik" id="nik" maxlength="10" />
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
     <input type="text" name="nama_karyawan" id="nama_karyawan" maxlength="50">
    </td>
  </tr>
  
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td>
      <input type="text" name="tempatlahir" id="tempatlahir" maxlength="50">
    </td>
    
  </tr>
  
  <tr>
    <td>Jenis Kelamin</td>
    <td>:</td>
    <td>
      <select name="jenis_kelamin" id="jenis_kelamin">
      <option value="L">Laki-Laki</option>
      <option value="P">Perempuan</option>
      </select>
    </td>
  </tr>
  
  
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td>
      <select name="tgl" id="tgl">
      <?php 
						for($tgl=1;$tgl<=31;$tgl++){
							if($tgl == $tgl_lahir[2]){
								$slc_tgl = 'SELECTED';
						}else{
							$slc_tgl = '';
						}
		?>
        <option <?=$slc_tgl;?> value="<?=$tgl;?>"><?=$tgl;?></option>
					<?php 
					  	}
					?>
      </select>
      <select name="bulan" id="bulan">
      <?php
							$n_bulan = array ('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','Oktober','September','November','Desember');
							
							for($bulan=0;$bulan<12;$bulan++){	
								if($bulan+1 == $tgl_lahir[1]){
								$slc_bln = 'SELECTED';
							}else{
							$slc_bln = '';
						}
					 ?>
						<option <?=$slc_bln;?> value="<?=$bulan+1;?>"><?=$n_bulan[$bulan];?> </option>
					 <?php
					        }
					  ?>	
      </select>
      <select name="tahun" id="tahun">
      <?php
							for($tahun=date('Y')-60;$tahun<=date('Y')-15;$tahun++){	
							if($tahun == $tgl_lahir[0]){
								$slc_thn = 'SELECTED';
						}else{
							$slc_thn = '';
						}
					  ?>
						<option  <?=$slc_thn;?> value="<?=$tahun;?>"><?=$tahun;?> </option>
					 <?php
					        }
					 ?>  
      </select>
    </td>
  </tr>
  
    <tr>
    <td>Telepon</td>
    <td>:</td>
    <td>
      <input type="text" name="telepon" id="telepon" maxlength="50">
    </td>
  </tr>
  
    <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5"></textarea>
    </td>
  </tr>
  

<tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      <label for="jabatan"></label>
       
      <select name="jabatan" id="jabatan">
      <?php foreach($data_jabatan as $data) { ?>
	  <option value="<?=$data->kode_jabatan; ?>">
      <?= $data->nama_jabatan; ?></option>
      <?php }?>
      </select>
    </td>
  </tr>
  
 
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="Simpan" id="Simpan" value="Simpan">
      <input type="reset" name="Batal" id="Batal" value="Batal">
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="Kembali Ke menu Selanjutnya" id="Kembali Ke menu Selanjutnya" value="Kembali Ke menu Selanjutnya">
    </td>
  </tr>
  
  
  
  
</table>
</form>